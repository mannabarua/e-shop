<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 7/18/2019
 * Time: 5:17 PM
 */

namespace Bitm\Product;

use Bitm\Utility\Utility;
use Bitm\Utility\Debugger;
use Bitm\Db\Db;
use PDO;

class Product
{
    public $id = "";

    public $brand_id = "";
    public $label_id = "";
    public $title = "";

    public $picture = "";
    public $short_description = "";
    public $description = "";
    public $total_sales = "";
    public $product_type = "";
    public $is_new = "";
    public $cost = "";
    public $mrp = "";
    public $special_price = "";
    public $soft_delete = "";
    public $is_draft = "";
    public $is_active = "";
    public $created_at = "";
    public $modified_at = "";


    public $conn = null;




    public function __construct()
    {
        $this->conn = Db::connect();
    }

    function index(){

        $query = "SELECT * FROM `products`";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $_products = $stmt->fetchAll();
        return $_products;

    }

    function show($id){

        $query = "SELECT * FROM `products` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $_product = $stmt->fetch();
        return $_product;
    }

    function view($id){

        $query = "SELECT * FROM `products` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $_product = $stmt->fetch();


        return $_product;
    }

    function edit($id){

        $query = "SELECT * FROM `products` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $products = $stmt->fetch();
        return $products;
    }


    public function update($id,$data)
    {

        $query = "UPDATE products SET brand_id=:brand_id,label_id=:label_id,title=:title,picture=:picture,short_description=:short_description,description=:description,total_sales=:total_sales,product_type=:product_type,is_new=:is_new,cost=:cost,mrp=:mrp,special_price=:special_price
                  WHERE id = :id";

//        $stmt = $this->conn->prepare($query);
////        $stmt->setFetchMode(PDO::FETCH_ASSOC);
//        $stmt->bindParam(':id',$id);
////
//
////        $stmt->execute();
////
////        $product = $stmt->fetch();
////
////        return $product;
//
//        $result = $stmt->execute($data);
//        return $result;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id',$id);
        $stmt->bindparam(":brand_id",$data['brand_id']);
        $stmt->bindparam(":label_id",$data['label_id']);
        $stmt->bindparam(":title",$data['title']);
        $stmt->bindparam(":picture",$data['picture']);
        $stmt->bindparam(":short_description",$data['short_description']);
        $stmt->bindparam(":description",$data['description']);

        $stmt->bindparam(":total_sales",$data['total_sales']);
        $stmt->bindparam(":product_type",$data['product_type']);
        $stmt->bindparam(":is_new",$data['is_new']);
        $stmt->bindparam(":cost",$data['cost']);
        $stmt->bindparam(":mrp",$data['mrp']);
        $stmt->bindparam(":special_price",$data['special_price']);

//        $stmt->bindparam(":soft_delete",$data['soft_delete']);
//        $stmt->bindparam(":is_active",$data['is_active']);
//        $stmt->bindparam(":is_draft",$data['is_draft']);
//
//        $stmt->bindparam(":created_at",$data['created_at']);
//        $stmt->bindparam(":modified_at",$data['modified_at']);

        $result = $stmt->execute();
        return $result;



    }



    public function store($data)
    {
        $query = "INSERT INTO products(brand_id,label_id,title,picture,short_description,description,total_sales,product_type,is_new,cost,mrp,special_price,soft_delete,is_active,is_draft,created_at,modified_at)
                             VALUES(:brand_id,:label_id,:title,:picture,:short_description,:description,:total_sales,:product_type,:is_new,:cost,:mrp,:special_price,:soft_delete,:is_active,:is_draft,:created_at,:modified_at)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":brand_id",$data['brand_id']);
        $stmt->bindparam(":label_id",$data['label_id']);
        $stmt->bindparam(":title",$data['title']);
        $stmt->bindparam(":picture",$data['picture']);
        $stmt->bindparam(":short_description",$data['short_description']);
        $stmt->bindparam(":description",$data['description']);

        $stmt->bindparam(":total_sales",$data['total_sales']);
        $stmt->bindparam(":product_type",$data['product_type']);
        $stmt->bindparam(":is_new",$data['is_new']);
        $stmt->bindparam(":cost",$data['cost']);
        $stmt->bindparam(":mrp",$data['mrp']);
        $stmt->bindparam(":special_price",$data['special_price']);

        $stmt->bindparam(":soft_delete",$data['soft_delete']);
        $stmt->bindparam(":is_active",$data['is_active']);
        $stmt->bindparam(":is_draft",$data['is_draft']);

        $stmt->bindparam(":created_at",$data['created_at']);
        $stmt->bindparam(":modified_at",$data['modified_at']);

        $result = $stmt->execute();
        return $result;

    }





    public function delete($id){

        $query = "DELETE FROM `products` WHERE `products`.`id` = :id";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id',$id);
        return $stmt->execute();

    }





}