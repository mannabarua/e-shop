<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 7/18/2019
 * Time: 5:17 PM
 */

namespace Bitm\Banner;

use Bitm\Utility\Utility;
use Bitm\Utility\Debugger;
use Bitm\Db\Db;
use PDO;

class Banner
{
//    public $id = "";
//
//    public $brand_id = "";
//    public $label_id = "";
//    public $title = "";
//
//    public $picture = "";
//    public $short_description = "";
//    public $description = "";
//    public $total_sales = "";
//    public $product_type = "";
//    public $is_new = "";
//    public $cost = "";
//    public $mrp = "";
//    public $special_price = "";
//    public $soft_delete = "";
//    public $is_draft = "";
//    public $is_active = "";
//    public $created_at = "";
//    public $modified_at = "";


    public $conn = null;




    public function __construct()
    {
        $this->conn = Db::connect();
    }

    function index(){

        $query = "SELECT * FROM `banners`";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $_banners = $stmt->fetchAll();
        return $_banners;

    }

    function show($id){

        $query = "SELECT * FROM `banners` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $_banner = $stmt->fetch();
        return $_banner;
    }

    function view($id){

        $query = "SELECT * FROM `banners` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $_banner = $stmt->fetch();


        return $_banner;
    }

    function edit($id){

        $query = "SELECT * FROM `banners` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $banners = $stmt->fetch();
        return $banners;
    }


    public function update($id,$data)
    {

        $query = "UPDATE banners SET title=:title,picture=:picture,link=:link,promotional_message=:promotional_message,html_banner=:html_banner
                  WHERE id = :id";


        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id',$id);

        $stmt->bindparam(":title",$data['title']);
        $stmt->bindparam(":picture",$data['picture']);
        $stmt->bindparam(":link",$data['link']);
        $stmt->bindparam(":promotional_message",$data['promotional_message']);
        $stmt->bindparam(":html_banner",$data['html_banner']);

//        $stmt->bindparam(":soft_delete",$data['soft_delete']);
//        $stmt->bindparam(":is_active",$data['is_active']);
//        $stmt->bindparam(":is_draft",$data['is_draft']);
//
//        $stmt->bindparam(":created_at",$data['created_at']);
//        $stmt->bindparam(":modified_at",$data['modified_at']);

        $result = $stmt->execute();
        return $result;



    }



    public function store($data)
    {
        $query = "INSERT INTO banners(title,picture,link,promotional_message,html_banner,is_active,is_draft,soft_delete,max_display,created_at,modified_at)
                             VALUES(:title,:picture,:link,:promotional_message,:html_banner,:is_active,:is_draft,:soft_delete,max_display,:created_at,:modified_at)";
        $stmt = $this->conn->prepare($query);

        $stmt->bindparam(":title",$data['title']);
        $stmt->bindparam(":picture",$data['picture']);
        $stmt->bindparam(":link",$data['link']);
        $stmt->bindparam(":promotional_message",$data['promotional_message']);
        $stmt->bindparam(":html_banner",$data['html_banner']);

        $stmt->bindparam(":is_active",$data['is_active']);
        $stmt->bindparam(":is_draft",$data['is_draft']);
        $stmt->bindparam(":soft_delete",$data['soft_delete']);

        $stmt->bindparam(":created_at",$data['created_at']);
        $stmt->bindparam(":modified_at",$data['modified_at']);

        $result = $stmt->execute();
        return $result;

    }





    public function delete($id){

        $query = "DELETE FROM `banners` WHERE `banners`.`id` = :id";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id',$id);
        return $stmt->execute();

    }





}