<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 7/18/2019
 * Time: 2:16 PM
 */

namespace Bitm\Utility;


class Utility
{
    const PROJECT_PATH = '/eshop-bitm/';

    const ADMIN_WEB = 'http://localhost/eshop-bitm/admin/';
    const ADMIN_WEBVIEW = 'http://localhost/eshop-bitm/admin/views/';
    const ADMIN_THEME = '/eshop-bitm/public/themes/coreui/';
    const ADMIN_ELEMENTS = '/eshop-bitm/admin/views/elements/';
    const ADMIN_LAYOUTS = '/eshop-bitm/admin/views/layouts/';
    const ADMIN_VIEWS = '/eshop-bitm/admin/views/';

    const FRONT_WEBURL = 'http://localhost/eshop-bitm/';
    const FRONT_ASSETS = 'http://localhost/eshop-bitm/public/themes/ecommerce/';
    const FRONT_ELEMENTS = '/eshop-bitm/front/views/elements/';
    const FRONT_WEBVIEW = 'http://localhost/eshop-bitm/front/views/';
    const FRONT_LAYOUTS = '/eshop-bitm/front/views/layouts/';




    static function redirect($path){
        header("location:$path");
    }

    static function getAdminElement($elementname){
        return $_SERVER['DOCUMENT_ROOT'].self::ADMIN_ELEMENTS.$elementname;
    }

    static function getFrontElement($elementname){
        return $_SERVER['DOCUMENT_ROOT'].self::FRONT_ELEMENTS.$elementname;
    }



}