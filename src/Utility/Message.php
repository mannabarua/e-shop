<?php

namespace Bitm\Utility;


class Message
{
    public function __construct(){
        session_start();
    }

    static function set($message){
        $_SESSION['message'] = $message;
    }


    static function get(){
        return $_SESSION['message'];
    }

    static function flush(){

        $_message = $_SESSION['message'];
        $_SESSION['message'] = null;
        return $_message;
    }

    static function hasMessage(){
        $_message = self::get();
        if(empty($_message)){
            return false;
        }
        return true;
    }

}