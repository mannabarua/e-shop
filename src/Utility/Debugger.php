<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 7/18/2019
 * Time: 2:19 PM
 */

namespace Bitm\Utility;


class Debugger
{
   static function d($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

   static function dd($var){
        echo "<pre>";
        print_r($var);
        echo "</pre>";
        die();
    }
}