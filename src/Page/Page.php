<?php
/**
 * Created by PhpStorm.
 * User: PHP Trainer
 * Date: 7/18/2019
 * Time: 9:00 PM
 */

namespace Bitm\Page;

use Bitm\Utility\Debugger;
use Bitm\Db\Db;
use PDO;

class Page
{
    public $id='';
    public $title = "";
    public $description = "";
    public $link = "";

    public $conn = null;

    public function __construct()
    {
        $this->conn = Db::connect();
    }

    function index(){

        $query = "SELECT * FROM `pages`";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $_pages = $stmt->fetchAll();
        return $_pages;

    }

//    function show($id){
//
//        $query = "SELECT * FROM `pages` WHERE id = :id";
//        $stmt = $this->conn->prepare($query);
//        $stmt->setFetchMode(PDO::FETCH_ASSOC);
//        $stmt->bindParam(':id',$id);
//        $stmt->execute();
//        $_page = $stmt->fetch();
//        return $_page;
//    }

    function view($id){

        $query = "SELECT * FROM `pages` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $_page = $stmt->fetch();


        return $_page;
    }

    function edit($id){

        $query = "SELECT * FROM `pages` WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $pages = $stmt->fetch();
        return $pages;
    }


    public function update($id,$data)
    {

        $query = "UPDATE pages SET title=:title,description=:description,link=:link
                  WHERE id = :id";

//        $stmt = $this->conn->prepare($query);
////        $stmt->setFetchMode(PDO::FETCH_ASSOC);
//        $stmt->bindParam(':id',$id);
////
//
////        $stmt->execute();
////
////        $product = $stmt->fetch();
////
////        return $product;
//
//        $result = $stmt->execute($data);
//        return $result;

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id',$id);

        $stmt->bindparam(":title",$data['title']);

        $stmt->bindparam(":description",$data['description']);


        $stmt->bindparam(":link",$data['link']);

//        $stmt->bindparam(":soft_delete",$data['soft_delete']);
//        $stmt->bindparam(":is_active",$data['is_active']);
//        $stmt->bindparam(":is_draft",$data['is_draft']);
//
//        $stmt->bindparam(":created_at",$data['created_at']);
//        $stmt->bindparam(":modified_at",$data['modified_at']);

        $result = $stmt->execute();
        return $result;



    }


    function store($data){

        $query = "INSERT INTO pages(title, description, link ) VALUES (:title, :description, :link)";

        $stmt = $this->conn->prepare($query);

        $stmt->bindparam(":title",$data['title']);
        $stmt->bindparam(":description",$data['description']);
        $stmt->bindparam(":link",$data['link']);

        $result = $stmt->execute();

        return $result;

    }

    public function delete($id){

        $query = "DELETE FROM `pages` WHERE `pages`.`id` = :id";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id',$id);
        return $stmt->execute();

    }


}