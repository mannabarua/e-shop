<footer class="app-footer">
    <div>
        <a href="https://coreui.io">Ecommerce</a>
        <span>&copy; 2019.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io">| Manna Barua</a>
    </div>
</footer>