<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');
use Bitm\Utility\Utility;
?>
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Ecommerce Admin </title>
<!--    <!-- Icons-->-->
<!--    <link href="node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">-->
<!--    <link href="node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">-->
<!--    <link href="node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">-->
<!--    <link href="node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">-->
    <!-- Main styles for this application-->
    <link href="<?php echo Utility::ADMIN_THEME;?>/css/style.css" rel="stylesheet">
    <link href="<?php echo Utility::ADMIN_THEME;?>vendors/pace-progress/css/pace.min.css" rel="stylesheet">
</head>

