<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Page\Page;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$page = new Page();
$page = $page->view($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>


<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Page</strong>View</div>
        <div class="card-body">

            <form class="form-horizontal">



                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title" value="<?=$page['title']?>" placeholder="Enter Title..">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="textarea-input">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="textarea-input" name="description" rows="9" placeholder="<?=$page['description']?>"></textarea>
                        <span class="help-block">Please enter page description </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Link</label>
                    <div class="col-md-9">
                        <input class="form-control" id="link" type="text" name="link" value="<?=$page['link']?>" placeholder="Enter Total Sales..">
                        <span class="help-block">Please enter Total Sales</span>
                    </div>
                </div>




            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>

