<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Page\Page;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$ppage = new Page();
$_page = $page->show($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>
<ul>
    <li><?=$_page['title']?></li>
    <li><?=$_page['description']?></li>
    <li><?=$_page['link']?></li>
</ul>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>