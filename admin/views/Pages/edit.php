<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Page\Page;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$page = new Page();
$_page = $page->edit($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>



<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Page</strong>Edit</div>
        <div class="card-body">

            <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="brand" type="text" name="title" value="<?php echo $_page['title']?>" placeholder="">
                        <span class="help-block">Please enter your brand</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="textarea-input">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="textarea-input" name="description" rows="9" placeholder="<?php echo $_page['description']?>"></textarea>
                        <span class="help-block">Please enter page description </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Link</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="link" value="<?php echo $_page['link']?>" placeholder="Enter Link..">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>




                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-dot-circle-o"></i>Update</button>
                    <!--                    <button class="btn btn-sm btn-danger" type="reset">-->
                    <!--                        <i class="fa fa-ban"></i> Reset</button>-->
                </div>
            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>

