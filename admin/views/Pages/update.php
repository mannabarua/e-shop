<?php
ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);

session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');


use Bitm\Utility\Utility;
use Bitm\Utility\Debugger;
use Bitm\Page\Page;
use Bitm\Utility\Validator;
use Bitm\Utility\Message;

$id = $_GET['id'];
$data = $_POST;

$page = new Page();
$result = $page->update($id,$data);


if($result){
    Message::set( "Page is updated successfully." );

}else{
    Message::set(  "There is a problem while updating page. Please try again later.");
}

Utility::redirect('index.php');


