<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Banner\Banner;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$banner = new Banner();
$_banner = $banner->edit($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>



<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Banner</strong>Edit</div>
        <div class="card-body">

            <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title" value="<?php echo $_banner['title']?>" placeholder="enter title">
                        <span class="help-block">Please enter title</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 col-form-label">Picture</label>
                    <div class="col-sm-9">
                        <input  type="file" name="picture" value="<?php echo $_banner['picture']?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="link">Link</label>
                    <div class="col-md-9">
                        <input class="form-control" id="link" type="text" name="link" value="<?php echo $_banner['link']?>" placeholder="Enter Link..">
                        <span class="help-block">Please enter banner link</span>
                    </div>
                </div>



                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="promotional_message">Promotional Message</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="promotional_message" name="promotional_message" rows="4" data-rule="required" placeholder="<?php echo $_banner['promotional_message']?>"></textarea>

                    </div>
                </div>



                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Html Banner</label>
                    <div class="col-md-9">
                        <input class="form-control" id="html_banner" type="text" name="html_banner" value="<?php echo $_banner['html_banner']?>" placeholder="Enter html banner">
                        <span class="help-block">Please enter Total Sales</span>
                    </div>
                </div>



<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="brand">Soft Delete</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="soft_delete" type="number" name="soft_delete" value="--><?php //echo $_product['soft_delete']?><!--" placeholder="Enter cost..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="label">Is Draft</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="is_draft" type="number" name="is_draft" value="--><?php //echo $_product['is_draft']?><!--" placeholder="Enter Mrp..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="title">Is Active</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="is_active" type="number" name="is_active" value="--><?php //echo $_product['is_active']?><!--" placeholder="Enter special_price..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->








                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-dot-circle-o"></i>Update</button>
<!--                    <button class="btn btn-sm btn-danger" type="reset">-->
<!--                        <i class="fa fa-ban"></i> Reset</button>-->
                </div>
            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>

