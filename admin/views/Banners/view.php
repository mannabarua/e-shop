<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Banner\Banner;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$banner = new Banner();
$banner = $banner->view($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>


<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Banner</strong> Create</div>
        <div class="card-body">

            <form class="form-horizontal">


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title"  value="<?=$banner['title']?>" placeholder="Enter Title..">
                        <span class="help-block">Please enter your title</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 col-form-label">Picture</label>
                    <div class="col-sm-9">
                        <input  type="file" name="picture" value="<?=$banner['picture']?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="link">Link</label>
                    <div class="col-md-9">
                        <input class="form-control" id="link" type="text" name="link" value="<?=$banner['link']?>" placeholder="Enter Link..">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="promotional_messgae">Promotional Messgae</label>
                    <div class="col-md-9">
                        <input class="form-control" id="promotional_messgae" type="text" name="promotional_messgae" value="<?=$banner['promotional_message']?>" placeholder="">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="html_banner">Html Banner</label>
                    <div class="col-md-9">
                        <input class="form-control" id="html_banner" type="text" name="html_banner" value="<?=$banner['html_banner']?>" placeholder="Enter Link..">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Is Active</label>
                    <div class="col-md-9">
                        <input class="form-control" id="is_active" type="number" name="is_active" value="<?=$banner['is_active']?>" placeholder="Enter special_price..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>

