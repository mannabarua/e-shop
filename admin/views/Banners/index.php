<?php
ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Banner\Banner;
use Bitm\Utility\Debugger;
use Bitm\Utility\Message;

$banner = new Banner();
$banner = $banner->index();



ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();
?>

<?php
ob_start();
?>
<?php

if(Message::hasMessage()):
    ?>

    <div class="alert alert-success" role="alert">
        <?php echo Message::flush('message'); ?>
    </div>

<?php
endif;
?>

<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Picture</th>
        <th scope="col">Link</th>
        <th scope="col">Promotional Message</th>
        <th scope="col">Html Banner</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
<?php



$_sl = 1;
foreach($banner as $b):
?>
    <tr>
        <th scope="row"><?php echo $_sl;?>
        </th>

        <td>
<!--            <a href="show.php?id=--><?php //echo $p['id']?><!--">--><?php //echo $p['title']."(".$p['id'].")";?>
<!--            </a>-->
            <?=$b['title'];?>
        </td>

        <td><?=$b['picture'];?>
        </td>
        <td><?php echo $b['link'];?>
        </td>
        <td><?php echo $b['promotional_message'];?>
        </td>
        <td><?php echo $b['html_banner'];?>
        </td>

        <td>  <a href="view.php?id=<?php echo $b['id']?>">View</a> | <a href="edit.php?id=<?php echo $b['id']?>">Edit</a> | <a href="delete.php?id=<?php echo $b['id']?>" onclick="return confirm('Are you sure you want to delete')">Delete </a> </td>
    </tr>
<?php
$_sl++;
endforeach;
?>

    </tbody>
</table>


<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>
