<?php
ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);

session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');


use Bitm\Utility\Utility;
use Bitm\Utility\Debugger;
use Bitm\Banner\Banner;
use Bitm\Utility\Validator;
use Bitm\Utility\Message;

$data = $_POST;

$banner = new Banner();
$result = $banner->store($data);

if($result){
    Message::set( "Banner is added successfully." );

}else{
    Message::set(  "There is a problem while adding banner. Please try again later.");
}

Utility::redirect('index.php');

