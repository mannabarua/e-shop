<?php
ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();
?>

<?php
ob_start();
?>


<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Banner</strong> Create</div>
        <div class="card-body">

            <form class="form-horizontal" action="store.php" method="post">


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title" placeholder="Enter title..">
                        <span class="help-block">Please enter title</span>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="picture" class="col-sm-3 col-form-label">Picture</label>
                    <div class="col-sm-9">
                        <input  type="file" name="picture">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">link</label>
                    <div class="col-md-9">
                        <input class="form-control" id="link" type="text" name="link" placeholder="Enter link">

                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Promotional Message</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="promotional_message" name="promotional_message" rows="4" data-rule="required" placeholder="promotional_message"></textarea>

                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="html_banner"> HTML Banner</label>
                    <div class="col-md-9">
                        <input class="form-control" id="html_banner" type="text" name="html_banner" placeholder="Enter html banner..">
                        <span class="help-block">Please enter Total Sales</span>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Is Active</label>
                    <div class="col-md-9">
                        <input class="form-control" id="is_active" type="number" name="is_active" placeholder="Enter special_price..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Is Draft</label>
                    <div class="col-md-9">
                        <input class="form-control" id="is_draft" type="number" name="is_draft" placeholder="Enter Mrp..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Soft Delete</label>
                    <div class="col-md-9">
                        <input class="form-control" id="soft_delete" type="number" name="soft_delete" placeholder="Enter soft delete">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Max  Display</label>
                    <div class="col-md-9">
                        <input class="form-control" id="max_display" type="number" name="max_display" placeholder="Enter max display">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Created At</label>
                    <div class="col-md-9">
                        <input class="form-control" id="created_at" type="date" name="created_at" placeholder="Enter special_price..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Modified At</label>
                    <div class="col-md-9">
                        <input class="form-control" id="modified_at" type="date" name="modified_at" placeholder="Enter special_price..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>





                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
<!--                    <button class="btn btn-sm btn-danger" type="reset">-->
<!--                        <i class="fa fa-ban"></i> Reset</button>-->
                </div>
            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>
