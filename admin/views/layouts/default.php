<?php
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
?>
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.15
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<?php include_once( Utility::getAdminElement("head.php") ); ?>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<?php include_once( Utility::getAdminElement("header.php") ); ?>
<div class="app-body">
    <?php include_once( Utility::getAdminElement("navigation.php") ); ?>
    <main class="main">
        <?php include_once( Utility::getAdminElement("breadcrumb.php") ); ?>
        <div class="container-fluid">
            <div class="animated fadeIn">
                ##CONTENT##
            </div>
        </div>
    </main>

</div>
<?php include_once( Utility::getAdminElement("footer.php") ); ?>
<?php include_once( Utility::getAdminElement("scripts.php") ); ?>

</body>
</html>