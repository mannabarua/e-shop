<?php
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
?>
<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.15
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<?php include_once( Utility::getAdminElement("head.php") ); ?>
  <body class="app flex-row align-items-center">
    <div class="container">
     ##CONTENT##
    </div>
    <?php include_once( Utility::getAdminElement("scripts.php") ); ?>
</html>
