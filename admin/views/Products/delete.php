<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Product\Product;
use Bitm\Utility\Debugger;
use Bitm\Utility\Message;


$id = $_GET['id'];

$product = new Product();
$result = $product->delete($id);

if($result){
    Message::set( "Product is deleted successfully." );

}else{
    Message::set(  "There is a problem while adding product. Please try again later.");
}

Utility::redirect('index.php');

