<?php
ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Product\Product;
use Bitm\Utility\Debugger;
use Bitm\Utility\Message;

$product = new Product();
$products = $product->index();



ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();
?>

<?php
ob_start();
?>
<?php

if(Message::hasMessage()):
    ?>

    <div class="alert alert-success" role="alert">
        <?php echo Message::flush('message'); ?>
    </div>

<?php
endif;
?>

<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Brand ID</th>
        <th scope="col">Label ID</th>
        <th scope="col">Description</th>
        <th scope="col">Picture</th>
        <th scope="col">Cost</th>
        <th scope="col">MRP</th>
        <th scope="col">Special Price</th>

        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
<?php



$_sl = 1;
foreach($products as $p):
?>
    <tr>
        <th scope="row"><?php echo $_sl;?>
        </th>
        <td>
<!--            <a href="show.php?id=--><?php //echo $p['id']?><!--">--><?php //echo $p['title']."(".$p['id'].")";?>
<!--            </a>-->
            <?=$p['title'];?>
        </td>

        <td><?=$p['brand_id'];?>
        </td>
        <td><?php echo $p['label_id'];?>
        </td>
        <td><?php echo $p['description'];?>
        </td>
        <td><?php echo $p['picture'];?>
        </td>
        <td><?php echo $p['cost'];?>
        </td>
        <td><?php echo $p['mrp'];?>
        </td>
        <td><?php echo $p['special_price'];?>
        </td>
        <td>  <a href="view.php?id=<?php echo $p['id']?>">View</a> | <a href="edit.php?id=<?php echo $p['id']?>">Edit</a> | <a href="delete.php?id=<?php echo $p['id']?>" onclick="return confirm('Are you sure you want to delete')">Delete </a> </td>
    </tr>
<?php
$_sl++;
endforeach;
?>

    </tbody>
</table>


<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>
