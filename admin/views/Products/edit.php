<?php

ini_set("display_errors","On");
error_reporting(E_ALL^E_NOTICE);
session_start();
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');

use Bitm\Utility\Utility;
use Bitm\Product\Product;
use Bitm\Utility\Debugger;


$id = $_GET['id'];

$product = new Product();
$_product = $product->edit($id);

ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].Utility::ADMIN_LAYOUTS.'/default.php');
$layout = ob_get_contents();
ob_end_clean();

?>



<?php
ob_start();
?>



<div class="col">
    <div class="card">
        <div class="card-header">
            <strong>Product</strong> Create</div>
        <div class="card-body">

            <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Brand ID</label>
                    <div class="col-md-9">
                        <input class="form-control" id="brand" type="text" name="brand_id" value="<?php echo $_product['brand_id']?>" placeholder="">
                        <span class="help-block">Please enter your brand</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Label ID</label>
                    <div class="col-md-9">
                        <input class="form-control" id="label" type="text" name="label_id" value="<?php echo $_product['label_id']?>"  >
                        <span class="help-block">Please enter your label</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Title</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title" value="<?php echo $_product['title']?>" placeholder="Enter Title..">
                        <span class="help-block">Please enter product title</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 col-form-label">Picture</label>
                    <div class="col-sm-9">
                        <input  type="file" name="picture" value="<?php echo $_product['picture']?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="short_description">Short Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="short_description" name="short_description" rows="4" data-rule="required" placeholder="<?php echo $_product['short_description']?>"></textarea>

                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="description" name="description" rows="4" data-rule="required" placeholder="<?php echo $_product['description']?>"></textarea>

                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Total Sales</label>
                    <div class="col-md-9">
                        <input class="form-control" id="total_sales" type="number" name="total_sales" value="<?php echo $_product['total_sales']?>" placeholder="Enter Total Sales..">
                        <span class="help-block">Please enter Total Sales</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">Product Type</label>
                    <div class="col-md-9">
                        <input class="form-control" id="product_type" type="text" name="product_type" value="<?php echo $_product['product_type']?>" placeholder="Enter Product Type..">
                        <span class="help-block">Please enter Product type</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Is New</label>
                    <div class="col-md-9">
                        <input class="form-control" id="is_new" type="text" name="is_new" value="<?php echo $_product['is_new']?>" placeholder="Enter Title..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="brand">Cost</label>
                    <div class="col-md-9">
                        <input class="form-control" id="cost" type="text" name="cost" value="<?php echo $_product['cost']?>" placeholder="Enter cost..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="label">MRP</label>
                    <div class="col-md-9">
                        <input class="form-control" id="mrp" type="text" name="mrp" value="<?php echo $_product['mrp']?>" placeholder="Enter Mrp..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="title">Special Price</label>
                    <div class="col-md-9">
                        <input class="form-control" id="special_price" type="text" name="special_price" value="<?php echo $_product['special_price']?>" placeholder="Enter special_price..">
                        <span class="help-block">Please enter </span>
                    </div>
                </div>

<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="brand">Soft Delete</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="soft_delete" type="number" name="soft_delete" value="--><?php //echo $_product['soft_delete']?><!--" placeholder="Enter cost..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="label">Is Draft</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="is_draft" type="number" name="is_draft" value="--><?php //echo $_product['is_draft']?><!--" placeholder="Enter Mrp..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="form-group row">-->
<!--                    <label class="col-md-3 col-form-label" for="title">Is Active</label>-->
<!--                    <div class="col-md-9">-->
<!--                        <input class="form-control" id="is_active" type="number" name="is_active" value="--><?php //echo $_product['is_active']?><!--" placeholder="Enter special_price..">-->
<!--                        <span class="help-block">Please enter </span>-->
<!--                    </div>-->
<!--                </div>-->








                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="submit">
                        <i class="fa fa-dot-circle-o"></i>Update</button>
<!--                    <button class="btn btn-sm btn-danger" type="reset">-->
<!--                        <i class="fa fa-ban"></i> Reset</button>-->
                </div>
            </form>
        </div>

    </div>

</div>





<?php
$pagecontent = ob_get_contents();
ob_end_clean();


echo str_replace("##CONTENT##", $pagecontent, $layout)

?>

