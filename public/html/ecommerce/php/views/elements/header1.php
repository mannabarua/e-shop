<header>

    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-3">

                </div>

                <div class="col-9 d-none d-lg-block">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">My Account</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">WishList</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Customer Care</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Company</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="signup.html">Sign Up</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login.html">Login</a>
                        </li>

                        <li class="nav-item search">
                            <a class="nav-link" href="#">
                                <input class="form-control" type="search" placeholder="Search Entire store here" aria-label="Search">
                                <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-9 d-lg-none">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-user ></i></a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fas fa-list"></i>
                                </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-headset"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-building"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fas fa-user-plus"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="far fa-sign-in-alt"></i></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-search"></i>
                            </a>
                        </li>



                    </ul>
                </div>
            </div>
        </div>
    </div>



    <div class="large">

        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">


                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item ">
                            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Laptop & NoteBook
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="hp.php">HP Laptop & Probook</a>
                                <a class="dropdown-item" href="acer.php">Acer Laptop & Notebook</a>
                                <a class="dropdown-item" href="asus.php">Asus Laptop & Notebook</a>
                                <a class="dropdown-item" href="apple.php">Apple Macbook</a>
                                <a class="dropdown-item" href="lenovo.php">lenovo Laptop & Notebook</a>
                                <a class="dropdown-item" href="dell.php">Dell Laptop & Notebook</a>
                                <a class="dropdown-item" href="gaming.php">Gaming Laptop</a>
                                <a class="dropdown-item" href="hm.php">Huawei Matebook</a>
                                <div class="dropdown-divider"></div>

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Gaming Peripherals
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="">Gaming Mouse</a>
                                <a class="dropdown-item" href="">Gaming Keyboard</a>
                                <a class="dropdown-item" href="">Gaming Headset</a>

                                <div class="dropdown-divider"></div>

                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Accessories
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="">Mouse</a>
                                <a class="dropdown-item" href="">Keyboard</a>
                                <a class="dropdown-item" href="">Headset</a>
                                <a class="dropdown-item" href="">Monitor</a>
                                <a class="dropdown-item" href="">Printer</a>
                                <a class="dropdown-item" href="">Sound System</a>


                                <div class="dropdown-divider"></div>

                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="LN.php">e-shop mall</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="LN.php">Global collection</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                all categories
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="LN.php">Laptop & Notebook</a>
                                <a class="dropdown-item" href="GP.php">Gaming Peripherals</a>
                                <a class="dropdown-item" href="A.php">Accessories</a>
                                <a class="dropdown-item" href="N.php">Networking Products</a>
                                <div class="dropdown-divider"></div>

                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="LN.html">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="LN.html">contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cart-plus"></i>
                                <span>(0)</span>
                            </a>
                        </li>


                    </ul>


                </div>





            </div>


        </nav>

    </div>

    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
           <div class="container">
               <div class="row">

               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
                   <span class="navbar-toggler-icon"></span>
               </button>

               <div class="collapse navbar-collapse" id="navbarResponsive">
                   <ul class="navbar-nav mr-auto">
                       <li class="nav-item active">
                           <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
                       </li>

                       <li class="nav-item dropdown">
                               <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 Product Category
                               </a>
                               <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                 <a class="dropdown-item" href="LN.html">Laptop & Notebook</a>
                                 <a class="dropdown-item" href="GP.html">Gaming Peripherals</a>
                                 <a class="dropdown-item" href="A.html">Accessories</a>
                                 <a class="dropdown-item" href="N.html">Networking Products</a>
                                 <div class="dropdown-divider"></div>

                               </div>
                             </li>

                       <li class="nav-item">
                           <a class="nav-link" href="#">CUSTOM</a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#"><i class="fas fa-pound-sign"></i></a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#"><i class="fas fa-euro-sign"></i>
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#"><i class="fas fa-dollar-sign"></i>
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#">
                               <img src="img/united-kingdom_flag.png" alt="united-kingdom flag" class="img-fluid">
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#">
                               <img src="img/flag-france.png" alt="France flag" class="img-fluid">
                           </a>
                       </li>
                       <li class="nav-item">
                           <a class="nav-link" href="#">
                               <img src="img/flag-germany.png" alt="Germany flag" class="img-fluid">
                           </a>
                       </li>

                       <li class="nav-item">
                           <a class="nav-link" href="#">
                               <i class="fas fa-cart-plus"></i>
                               <span>(0)</span>
                           </a>
                       </li>
                           <div>
                               <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                               <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

                           </div>

                   </ul>

               </div>
               </div>
           </div>
       </nav>-->

</header>