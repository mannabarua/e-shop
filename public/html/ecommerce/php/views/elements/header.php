<header>

<div class="small">


    <nav class="navbar navbar-expand-lg navbar-dark ">
        <div class="container-fluid">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">


            </ul>


            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search">
                <button class="btn btn-outline-success my-1 my-sm-0" type="submit">Search</button>
            </form>

        </div>
        </div>
    </nav>

</div>
    <div class="large">


            <div class="container-fluid">

    <ul class="nav justify-content-center">

        <li class="nav-item ">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Laptop & NoteBook
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="hp.php">HP Laptop & Probook</a>
                <a class="dropdown-item" href="acer.php">Acer Laptop & Notebook</a>
                <a class="dropdown-item" href="asus.php">Asus Laptop & Notebook</a>
                <a class="dropdown-item" href="apple.php">Apple Macbook</a>
                <a class="dropdown-item" href="lenovo.php">lenovo Laptop & Notebook</a>
                <a class="dropdown-item" href="dell.php">Dell Laptop & Notebook</a>
                <a class="dropdown-item" href="gaming.php">Gaming Laptop</a>
                <a class="dropdown-item" href="hm.php">Huawei Matebook</a>
                <div class="dropdown-divider"></div>

            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Gaming Peripherals
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="">Gaming Mouse</a>
                <a class="dropdown-item" href="">Gaming Keyboard</a>
                <a class="dropdown-item" href="">Gaming Headset</a>

                <div class="dropdown-divider"></div>

            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Accessories
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="">Mouse</a>
                <a class="dropdown-item" href="">Keyboard</a>
                <a class="dropdown-item" href="">Headset</a>
                <a class="dropdown-item" href="">Monitor</a>
                <a class="dropdown-item" href="">Printer</a>
                <a class="dropdown-item" href="">Sound System</a>


                <div class="dropdown-divider"></div>

            </div>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="LN.php">Global collection</a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                all categories
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="LN.php">Laptop & Notebook</a>
                <a class="dropdown-item" href="GP.php">Gaming Peripherals</a>
                <a class="dropdown-item" href="A.php">Accessories</a>
                <a class="dropdown-item" href="N.php">Networking Products</a>
                <div class="dropdown-divider"></div>

            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="LN.html">Services</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="LN.html">Contact</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="LN.php">log In/Sign Up</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-cart-plus"></i>
                <span>(0)</span>
            </a>
        </li>


    </ul>
            </div>


    </div>



</header>