<!doctype html>
<html lang="en">
        <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            
                <link rel="stylesheet" href="css/style1.css">
            
                <title>Classic</title>
            </head>
<body>


<!--Markup for Header-->

<?php include_once '../views/elements/header.php'  ?>

<!--Markup for Card-->
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-2 ">

            <a class="my-4">
                <img src="img/logo1.png" alt="Logo icon" class="img-fluid">
            </a>
            <h4>Category</h4>
            <div class="list">
                <a href="LN.php" class="list">Laptop and Notebook</a><br>
                <a href="GP.php" class="list-group-item-action">Gaming Peripherls</a><br>
                <a href="A.php" class="list">Accessories</a><br>
                <a href="N.php" class="list">Networking Products</a><br>
            </div>

        </div>
        <div class="col-lg-10 ">
            <div class="card bg-light text-dark">
                <img src="img/game3.jpg" class="card-img" alt="laptop">
                <div class="card-img-overlay">
                    <h3 class="card-title">ALL Gaming Peripherals</h3>
                    <p class="card-text">This is a page for all <br>gaming peripherals.</p>

                </div>
            </div>
        </div>



    </div>
</div>

<br>

 <!---<section id="newProducts">
<div class="container">
    <div class="row">
        <div class="col-lg-0">

        </div>
         <div class="col-lg-12">
                <div class="card-deck">
                        <div class="card">
                            <img src="img/dell.jpg" class="card-img-top" alt="dell laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/lenovo.jpg" class="card-img-top" alt="lenovo laptop">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                        </div>
                        <div class="card">
                            <img src="img/asus.jpg" class="card-img-top" alt="asus laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="img/apple.jpg" class="card-img-top" alt="apple laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="img/acer.jpg" class="card-img-top" alt="acer laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
            
                          </div> 
         </div>          
    </div>

</div>
</section>-->

<div class="container">

    <h1> Gaming Peripherals</h1>

</div>

<section id="newProducts">
  <div class="container">
      <div class="row">
          <div class="col-lg-0">
              
          </div>
           <div class="col-lg-12">
                  <div class="card-deck">
                      <div class="card">
                        <img src="img/gm1.jpg" alt="hp laptop">
                          <div class="card-body">
                           
                          gaming mouse BDT 1000
                          </div>
                          <div class="card-footer">
                              
                              <a href="viewgm.html" class="btn btn-primary">view Details</a>
                              <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                            
                          </div>
                        </div>
                        <div class="card">
                          <img src="img/gm2.jpg" alt="hp laptop">
                            <div class="card-body">
                             
                            gaming mouse BDT 1000
                            </div>
                            <div class="card-footer">
                                
                                <a href="viewgm.html" class="btn btn-primary">view Details</a>
                                <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                              
                            </div>
                          </div>
                          <div class="card">
                            <img src="img/gm3.jpg" alt="hp laptop">
                              <div class="card-body">
                               
                              gaming mouse BDT 1000
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="viewgm.html" class="btn btn-primary">view Details</a>
                                  <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                
                              </div>
                            </div>
                            <div class="card">
                              <img src="img/gm1.jpg" alt="hp laptop">
                                <div class="card-body">
                                 
                                gaming mouse BDT 1000
                                </div>
                                <div class="card-footer">
                                    
                                    <a href="viewgm.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                  
                                </div>
                              </div>
                          
              
                            </div> 
           </div>          
      </div>
  
  </div>
  </section>
  <br>
  <section id="newProducts">
      <div class="container">
          <div class="row">
              <div class="col-lg-0">
                  
              </div>
               <div class="col-lg-12">
                      <div class="card-deck">
                          <div class="card">
                              <img src="img/k1.jpg" class="card-img-top" alt="hp laptop">
                              <div class="card-body">
                               
                                gaming keyboard BDT 1500
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="viewgm1.html" class="btn btn-primary">view Details</a>
                                  <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                
                              </div>
                            </div>
                            <div class="card">
                              <img src="img/k3.jpg" class="card-img-top" alt="hp laptop">
                              <div class="card-body">
                               
                                gaming keyboard BDT 1500
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="viewgm1.html" class="btn btn-primary">view Details</a>
                                  <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                
                              </div>
                            </div>
                            <div class="card">
                              <img src="img/k2.jpg" class="card-img-top" alt="hp laptop">
                              <div class="card-body">
                               
                                gaming keyboard BDT 1500
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="viewgm1.html" class="btn btn-primary">view Details</a>
                                  <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                
                              </div>
                            </div>
                            <div class="card">
                              <img src="img/k3.jpg" class="card-img-top" alt="hp laptop">
                              <div class="card-body">
                               
                                gaming keyboard BDT 1500
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="viewgm1.html" class="btn btn-primary">view Details</a>
                                  <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                
                              </div>
                            </div>
                              
                  
                  </div> 
               </div>          
          </div>
      
      </div>
  </section>
  <br>
  <section id="newProducts">
          <div class="container">
              <div class="row">
                  <div class="col-lg-0">
                      
                  </div>
                   <div class="col-lg-12">
                          <div class="card-deck">
                              <div class="card">
                                  <img src="img/gh.jpg" class="card-img-top" alt="hp laptop">
                                  <div class="card-body">
                                   
                                    gaming headset 1200 BDT
                                  </div>
                                  <div class="card-footer">
                                      
                                      <a href="viewgh.html" class="btn btn-primary">view Details</a>
                                      <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    
                                  </div>
                                </div>
                               
                                <div class="card">
                                  <img src="img/gh1.jpg" class="card-img-top" alt="hp laptop">
                                  <div class="card-body">
                                   
                                    gaming headset 1200 BDT
                                  </div>
                                  <div class="card-footer">
                                      
                                      <a href="viewgh.html" class="btn btn-primary">view Details</a>
                                      <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    
                                  </div>
                                </div>
                               
                                <div class="card">
                                  <img src="img/gh2.jpg" class="card-img-top" alt="hp laptop">
                                  <div class="card-body">
                                   
                                    gaming headset 1200 BDT
                                  </div>
                                  <div class="card-footer">
                                      
                                      <a href="viewgh.html" class="btn btn-primary">view Details</a>
                                      <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    
                                  </div>
                                </div>
                                <div class="card">
                                  <img src="img/gh3.jpg" class="card-img-top" alt="hp laptop">
                                  <div class="card-body">
                                   
                                    gaming headset 1200 BDT
                                  </div>
                                  <div class="card-footer">
                                      
                                      <a href="viewgh.html" class="btn btn-primary">view Details</a>
                                      <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    
                                  </div>
                                </div>
                               
                                  
                      
                      </div> 
                   </div>          
              </div>
          
          </div>
  </section>

<!-- footer-->
<?php include_once '../views/elements/footer.php'  ?>


<?php include_once '../views/elements/script.php'  ?>


</body>
</html>