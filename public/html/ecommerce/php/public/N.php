<!doctype html>
<html lang="en">
        <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            
                <link rel="stylesheet" href="css/style1.css">
            
                <title>Classic</title>
            </head>
<body>


<!--Markup for Header-->
<?php include_once '../views/elements/header.php'  ?>



<!--Markup for Card-->
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-2 ">

            <a class="my-4">
                <img src="img/logo1.png" alt="Logo icon" class="img-fluid">
            </a>
            <h4>Category</h4>
            <div class="list">
                <a href="LN.php" class="list">Laptop and Notebook</a><br>
                <a href="GP.php" class="list">Gaming Peripherls</a><br>
                <a href="A.php" class="list">Accessories</a><br>
                <a href="N.php" class="list-group-item-action">Networking Products</a><br>
            </div>

        </div>
        <div class="col-lg-10 ">
            <div class="card bg-light text-dark">
                <img src="img/network.jpg" class="card-img" alt="laptop">
                <div class="card-img-overlay">
                    <h3 class="card-title">ALL Networking Products</h3>
                    <p class="card-text">This is a page for all <br>Networking Products.</p>

                </div>
            </div>
        </div>



    </div>
</div>

<br>
 <!---<section id="newProducts">
<div class="container">
    <div class="row">
        <div class="col-lg-0">

        </div>
         <div class="col-lg-12">
                <div class="card-deck">
                        <div class="card">
                            <img src="img/dell.jpg" class="card-img-top" alt="dell laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/lenovo.jpg" class="card-img-top" alt="lenovo laptop">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                        </div>
                        <div class="card">
                            <img src="img/asus.jpg" class="card-img-top" alt="asus laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="img/apple.jpg" class="card-img-top" alt="apple laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="img/acer.jpg" class="card-img-top" alt="acer laptop">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                </div>
                            </div>
            
                          </div> 
         </div>          
    </div>

</div>
</section>-->
<div class="container">

    <h1> Networking Products</h1>

</div>

<section id="newProducts1">
            <div class="container">
               
                    <div class="card-group">
    
                            <div class="card">
                              <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                              <div class="card-body">
                               
                                <img src="img/hp1.jpg" alt="hp laptop">
                              </div>
                              <div class="card-footer">
                                  
                                  <a href="#" class="btn btn-primary">All HP Laptop & Notebook</a>
                                <small class="text-muted">Last updated 3 mins ago</small>
                              </div>
                            </div>
    
                            <div class="card">
                              <img src="img/acer.jpg" class="card-img-top" alt="acer laptop">
                              <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This card has</p>
                              </div>
                              <div class="card-footer">
                                <small class="text-muted">Last updated 3 mins ago</small>
                              </div>
                            </div>
    
                            <div class="card">
                              <img src="img/asus.jpg" class="card-img-top" alt="asus laptop">
                              <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">lead-in to additional content. This card h</p>
                              </div>
                              <div class="card-footer">
                                <small class="text-muted">Last updated 3 mins ago</small>
                              </div>
                            </div>
    
                            <div class="card">
                                    <img src="img/apple.jpg" class="card-img-top" alt="apple note book">
                                    <div class="card-body">
                                      <h5 class="card-title">Card title</h5>
                                      <p class="card-text">This is a wider card with supporting text below as a n</p>
                                    </div>
                                    <div class="card-footer">
                                      <small class="text-muted">Last updated 3 mins ago</small>
                                    </div>
                            </div>
    
                            
                             
                    </div> 
            </div>
</section>

<section id="newProducts1">
    <div class="container">
  <div class="card-group">
            <div class="card">
                <img src="img/lenovo.jpg" class="card-img-top" alt="lenovo laptop">
                <div class="card-body">
                  <h5 class="card-title">Card title</h5>
                  <p class="card-text">This is a wider card with supporting text below</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">Last updated 3 mins ago</small>
                </div>
    </div>
    
    <div class="card">
        <img src="img/dell.jpg" class="card-img-top" alt="lenovo laptop">
        <div class="card-body">
          <h5 class="card-title">Card title</h5>
          <p class="card-text">This is a wider card with supporting text below</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Last updated 3 mins ago</small>
        </div>
</div>
<div class="card">
    <img src="img/game.jpg" class="card-img-top" alt="lenovo laptop">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">Last updated 3 mins ago</small>
    </div>
</div>
<div class="card">
    <img src="img/huawei1.jpg" class="card-img-top" alt="lenovo laptop">
    <div class="card-body">
      <h5 class="card-title">Card title</h5>
      <p class="card-text">This is a wider card with supporting text below</p>
    </div>
    <div class="card-footer">
      <small class="text-muted">Last updated 3 mins ago</small>
    </div>
</div>
</div>


      </div>
</section>

<br>
<!-- footer-->
<?php include_once '../views/elements/footer.php'  ?>


<?php include_once '../views/elements/script.php'  ?>

</body>
</html>