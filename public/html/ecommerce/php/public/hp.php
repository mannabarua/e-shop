<!doctype html>
<html lang="en">
        <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            
                <link rel="stylesheet" href="css/style.css">
            
                <title>Classic</title>
            </head>
<body>


<!--Markup for Header-->

<?php include_once '../views/elements/header.php'  ?>

<!--Markup for Card-->
<div class="container-fluid">
        <div class="row">
    
                <div class="col-lg-2 ">
    
                        <a class="my-4">
                                <img src="img/logo1.png" alt="Logo icon" class="img-fluid">
                        </a>
                        <h5>Category</h5>
                        <div class="list">
                          <a href="LN.php" class="list">Laptop and Notebook</a>
                          <a href="GP.php" class="list">Gaming Peripherls</a>
                          <a href="A.php" class="list">Accessories</a><br>
                          <a href="N.php" class="list">Networking Products</a>
                        </div>
                
                </div>
                <!-- /.col-lg-3 -->

                <div class="col-lg-10 ">
                    <!--<section class="newProducts">

                        <div class="card-deck">
                            <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">

                                    <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">

                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="add-to-cart.html" class="btn btn-primary">Buy Now!</a>
                                    <small class="text-muted">HP Probook</small>
                                </div>
                            </div>
                            <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">

                                    <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">

                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                            <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">

                                    <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">

                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                            <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">

                                    <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">

                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>


                        </div>

                    </section>-->
                    <div class="card-group">
                        <div class="card">
                            <img class="card-img-top" src="img/hpp1.jpg" alt="Card image cap">


                        </div>
                        <div class="card">
                            <img class="card-img-top" src="img/hpp2.jpg" alt="Card image cap">


                        </div>
                        <div class="card">
                            <img class="card-img-top" src="img/hpp3.jpg" alt="Card image cap">


                        </div>
                    </div>

                </div>
                <!-- /.col-lg-3 -->
        </div>
    </div>
<br>

 <section id="newProducts">
<div class="container">
    <div class="row">
        <div class="col-lg-0">
            
        </div>
         <div class="col-lg-12">
                <div class="card-deck">
                    <div class="card">
                        <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">
                         
                          <img src="img/hp1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">
                            
                            <a href="viewhp.html" class="btn btn-primary">view Details</a>
                            <a href="add-to-cart.html" class="btn btn-primary">Buy Now!</a>
                          <small class="text-muted">HP Probook</small>
                        </div>
                      </div>
                      <div class="card">
                        <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">
                         
                          <img src="img/hp1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">
                            
                            <a href="viewhp.html" class="btn btn-primary">view Details</a>
                            <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                          <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                      </div>
                      <div class="card">
                        <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">
                         
                          <img src="img/hp1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">
                            
                            <a href="viewhp.html" class="btn btn-primary">view Details</a>
                            <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                          <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                      </div>
                      <div class="card">
                        <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">
                        
                          <img src="img/hp1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">
                            
                            <a href="viewhp.html" class="btn btn-primary">view Details</a>
                            <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                          <small class="text-muted">Last updated 3 mins ago</small>
                        </div>
                      </div>
                        
            
                          </div> 
         </div>          
    </div>

</div>
</section>
<br>
<section id="newProducts">
    <div class="container">
        <div class="row">
            <div class="col-lg-0">
                
            </div>
             <div class="col-lg-12">
                    <div class="card-deck">
                        <div class="card">
                            <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                            <div class="card-body">
                             
                              <img src="img/hp1.jpg" alt="hp laptop">
                            </div>
                            <div class="card-footer">
                                
                                <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                              <small class="text-muted">HP Probook</small>
                            </div>
                          </div>
                          <div class="card">
                            <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                            <div class="card-body">
                             
                              <img src="img/hp1.jpg" alt="hp laptop">
                            </div>
                            <div class="card-footer">
                                
                                <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                              <small class="text-muted">Last updated 3 mins ago</small>
                            </div>
                          </div>
                          <div class="card">
                            <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                            <div class="card-body">
                             
                              <img src="img/hp1.jpg" alt="hp laptop">
                            </div>
                            <div class="card-footer">
                                
                                <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                              <small class="text-muted">Last updated 3 mins ago</small>
                            </div>
                          </div>
                          <div class="card">
                            <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                            <div class="card-body">
                            
                              <img src="img/hp1.jpg" alt="hp laptop">
                            </div>
                            <div class="card-footer">
                                
                                <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                              <small class="text-muted">Last updated 3 mins ago</small>
                            </div>
                          </div>
                            
                
                </div> 
             </div>          
        </div>
    
    </div>
</section>
<br>
<section id="newProducts">
        <div class="container">
            <div class="row">
                <div class="col-lg-0">
                    
                </div>
                 <div class="col-lg-12">
                        <div class="card-deck">
                            <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                 
                                  <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">
                                    
                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                  <small class="text-muted">HP Probook</small>
                                </div>
                              </div>
                              <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                 
                                  <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">
                                    
                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                  <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                              </div>
                              <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                 
                                  <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">
                                    
                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                  <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                              </div>
                              <div class="card">
                                <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                                <div class="card-body">
                                
                                  <img src="img/hp1.jpg" alt="hp laptop">
                                </div>
                                <div class="card-footer">
                                    
                                    <a href="viewhp.html" class="btn btn-primary">view Details</a>
                                    <a href="buyhp.html" class="btn btn-primary">Buy Now!</a>
                                  <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                              </div>
                                
                    
                    </div> 
                 </div>          
            </div>
        
        </div>
</section>



<br>
<!-- footer-->
<?php include_once '../views/elements/footer.php'  ?>


<?php include_once '../views/elements/script.php'  ?>

</body>
</html>