<!doctype html>
<html lang="en">
        <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            
                <link rel="stylesheet" href="css/style.css">
            
                <title>Classic</title>
            </head>
<body>


<!--Markup for Header-->

<?php include_once '../views/elements/header.php'  ?>


<!--Markup for Card-->
<div class="container-fluid">
        <div class="row">
    
                <div class="col-lg-2 ">
    
                        <a class="my-4">
                                <img src="img/logo1.png" alt="Logo icon" class="img-fluid">
                        </a>
                        <h5>Category</h5>
                        <div class="list">
                          <a href="LN.html" class="list">Laptop and Notebook</a>
                          <a href="GP.html" class="list">Gaming Peripherls</a>
                          <a href="A.html" class="list">Accessories</a><br>
                          <a href="N.html" class="list">Networking Products</a>
                        </div>
                
                </div>
                <!-- /.col-lg-3 -->
    
                <div class="col-lg-10 ">
                        <div class="card mb-3" style="width: 1060px; height: 400px;">
                                <div class="row no-gutters">
                                  <div class="col-md-6">
                                    <img src="img/gamek.jpg" width="100px" height="350px" class="card-img" alt="laptop">
                                  </div>
                                  <div class="col-md-6">
                                    <div class="card-body">
                                      <h3 class="card-title"> HP Laptop & Notebook</h3>
                                      <h5 class="card-title"> 50k BDT</h5>
                                      <p class="card-text">This is a View HP Laptop
                                         page for HP Laptop & Notebook .HP Pavilion is a line of personal computers produced by Hewlett-Packard and introduced in 1995. The name is applied to both desktops and laptops for the Home and Home Office product range. The Pavilion mainly competes against computers such as Acer's Aspire, Dell's Inspiron and XPS, Lenovo's IdeaPad and Toshiba's Satellite.

                                         When HP merged with Compaq in 2002, it took over Compaq's existing naming rights agreement. As a result, HP sold both HP and Compaq-branded machines until 2013. </p>
                                      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                      <a href="" class="btn btn-primary">Add to Cart</a>
                                    <a href="" class="btn btn-primary">Buy Now!</a>
                                    </div>
                                  </div>
                                </div>
                                
                                    
                                    
                                  
                                
                              </div>
                </div>
                <!-- /.col-lg-3 -->
        </div>
    </div>
<br>

<!-- footer-->
<?php include_once '../views/elements/footer.php'  ?>

<?php include_once '../views/elements/script.php'  ?>
</body>
</html>

