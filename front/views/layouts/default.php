<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');
use Bitm\Utility\Utility;
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<?php  include_once (Utility::getFrontElement('head.php'));?>
<body>
<a href="offer.html"><img src="<?php echo Utility::FRONT_ASSETS?>/images/download.png" class="img-head" alt=""></a>
<div class="header">

    <div class="container">

        <div class="logo">
            <h1 ><a href="index.html"><b>T<br>H<br>E</b>Big Store<span>The Best Supermarket</span></a></h1>
        </div>

        <?php  include_once (Utility::getFrontElement('navigation-top.php'));?>

        <?php  include_once (Utility::getFrontElement('social.php'));?>

        <?php  include_once (Utility::getFrontElement('navigation.php'));?>



    </div>
</div>







<!--content-->
<div class="product">
    <div class="container">
        <div class="spec ">
            <h3>##PageTitle##</h3>
            <div class="ser-t">
                <b></b>
                <span><i></i></span>
                <b class="line"></b>
            </div>
        </div>
        <div>
            ##PageContent##
        </div>
    </div>
</div>


<?php  include_once (Utility::getFrontElement('footer.php'));?>

<!-- smooth scrolling -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */
        $().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<!-- for bootstrap working -->
<script src="<?=Utility::FRONT_ASSETS?>/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<script type='text/javascript' src="<?=Utility::FRONT_ASSETS?>/js/jquery.mycart.js"></script>
<script type="text/javascript">
    $(function () {

        var goToCartIcon = function($addTocartBtn){
            var $cartIcon = $(".my-cart-icon");
            var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
            $addTocartBtn.prepend($image);
            var position = $cartIcon.position();
            $image.animate({
                top: position.top,
                left: position.left
            }, 500 , "linear", function() {
                $image.remove();
            });
        }

        $('.my-cart-btn').myCart({
            classCartIcon: 'my-cart-icon',
            classCartBadge: 'my-cart-badge',
            affixCartIcon: true,
            checkoutCart: function(products) {
                $.each(products, function(){
                    console.log(this);
                });
            },
            clickOnAddToCart: function($addTocart){
                goToCartIcon($addTocart);
            },
            getDiscountPrice: function(products) {
                var total = 0;
                $.each(products, function(){
                    total += this.quantity * this.price;
                });
                return total * 1;
            }
        });

    });
</script>

<?php  include_once (Utility::getFrontElement('modals.php'));?>

</body>
</html>