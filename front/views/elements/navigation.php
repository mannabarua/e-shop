<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/eshop-bitm/vendor/autoload.php');
use Bitm\Utility\Utility;
?>
<div class="nav-top">
    <nav class="navbar navbar-default">

        <div class="navbar-header nav_2">
            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


        </div>
        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
            <ul class="nav navbar-nav ">
                <li class=" active"><a href="<?php echo Utility::FRONT_WEBURL?>" class="hyper "><span>Home</span></a></li>


                <li><a href="codes.html" class="hyper"> <span>Codes</span></a></li>
                <li><a href="<?=Utility::FRONT_WEBVIEW?>Static/contact.php" class="hyper"><span>Contact Us</span></a></li>
            </ul>
        </div>
    </nav>
    <div class="cart" >

        <span class="fa fa-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span>
    </div>
    <div class="clearfix"></div>
</div>