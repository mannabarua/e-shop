



<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../public/themes/ecommerce/css/style.css">

    <title>Ecommerce</title>
</head>
<body>

<!--Markup for Header-->

<?php include_once '../public/html/ecommerce/php/views/elements/header.php'  ?>


<!--Markup for Carousel-->

<div class="container-fluid">
    <div class="row">

        <div class="col-lg-2">

            <a class="my-4" href="">
                <img src="../public/html/ecommerce/php/public/img/logo1.png" alt="Logo icon" class="img-fluid">
            </a>
            <h4>Category</h4>


            <div class="list">
                <a href="LN.php" class="list">Laptop and Notebook</a><br>
                <a href="GP.php" class="list">Gaming Peripherls</a><br>
                <a href="A.php" class="list">Accessories</a><br>
                <a href="N.php" class="list">Networking Products</a>

            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-10">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>
                    <li data-target="#myCarousel" data-slide-to="6"></li>
                    <li data-target="#myCarousel" data-slide-to="7"></li>
                    <li data-target="#myCarousel" data-slide-to="8"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="../public/html/ecommerce/php/public/img/m1.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m2.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m3.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m4.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m5.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m6.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m7.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m8.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="../public/html/ecommerce/php/public/img/m9.jpg" class="d-block w-100" alt="Slider Image">
                        <div class="carousel-caption d-none d-md-block">

                        </div>
                    </div>
                </div>


                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- /.col-lg-3 -->
    </div>
</div>
<br>

<!--Markup for Services-->

<!--<section id="services">-->
<!--    <div class="container">-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-md-4">-->
<!--                <div>-->
<!---->
<!--                    <a class="nav-link" href="#"><i class="fas fa-plane-departure"></i></a>-->
<!---->
<!--                </div>-->
<!--                <div>-->
<!--                    <h5>FREE SHIPPING</h5>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div>-->
<!--                    <a class="nav-link" href="#"><i class="fas fa-plane-departure"></i></a>-->
<!---->
<!--                </div>-->
<!--                <div>-->
<!--                    <h5>FREE SHIPPING</h5>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <div>-->
<!--                    <a class="nav-link" href="#"><i class="fas fa-plane-departure"></i></a>-->
<!---->
<!--                </div>-->
<!--                <div>-->
<!--                    <h5>FREE SHIPPING</h5>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!---->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<!--Markup for New Products-->
<section id="newProducts">
    <div class="container">
        <h4> NoteBook & Laptop</h4>
        <div class="row">
            <div class="col-lg-3">


                <div class="card" style="width: 17rem;">


                    <img class="card-img-top" src="img/ln1.jpg" alt="Card image cap">
                    <div class="card-body">

                        <center>
                            <a href="LN.php" class="btn btn-primary">SHOP NOW!</a>
                        </center>
                    </div>
                </div>

                <br>
            </div>
            <div class="col-lg-9">
                <div class="card-deck">
                    <div class="card">
                        <img src="img/hp.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">

                            <img src="img/hp1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">

                            <a href="viewhp.php" class="btn btn-primary">View Details</a>
                            <a href="add-to-cart.php" class="btn btn-primary">Buy Now!</a>
                            <small class="text-muted">hp Probook</small>
                        </div>
                    </div>
                    <div class="card">
                        <img src="img/acer.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">

                            <img src="img/acer1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">

                            <a href="viewhp.php" class="btn btn-primary">view Details</a>
                            <a href="buyhp.php" class="btn btn-primary">Buy Now!</a>
                            <small class="text-muted">Acer Probook</small>
                        </div>
                    </div>
                    <div class="card">
                        <img src="img/asus.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">

                            <img src="img/asus1.jpg" alt="hp laptop">
                        </div>
                        <div class="card-footer">

                            <a href="viewhp.php" class="btn btn-primary">view Details</a>
                            <a href="buyhp.php" class="btn btn-primary">Buy Now!</a>
                            <small class="text-muted">ASUS Probook</small>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<br>
<section id="newProducts">
    <div class="container">
        <h4>Gaming Peripherals</h4>
        <div class="row">
            <div class="col-lg-3">

                <div class="card" style="width: 17rem;">

                    <img class="card-img-top" src="img/g1.jpg" alt="Card image cap">
                    <div class="card-body">



                        <a href="GP.php" class="btn btn-primary">SHOP NOW!</a>
                    </div>
                </div>

                <br>
            </div>
            <div class="col-lg-9">
                <div class="card-deck">
                    <div class="card">
                        <img src="img/gm1.jpg" alt="hp laptop">
                        <div class="card-body">

                            gaming mouse BDT 1000
                        </div>
                        <div class="card-footer">

                            <a href="viewgm.php" class="btn btn-primary">view Details</a>
                            <a href="buyhp.php" class="btn btn-primary">Buy Now!</a>

                        </div>
                    </div>
                    <div class="card">
                        <img src="img/k2.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">

                            gaming keyboard BDT 1500
                        </div>
                        <div class="card-footer">

                            <a href="viewgm1.php" class="btn btn-primary">view Details</a>
                            <a href="buyhp.php" class="btn btn-primary">Buy Now!</a>

                        </div>
                    </div>
                    <div class="card">
                        <img src="img/gh.jpg" class="card-img-top" alt="hp laptop">
                        <div class="card-body">

                            gaming headset 1200 BDT
                        </div>
                        <div class="card-footer">

                            <a href="viewgh.php" class="btn btn-primary">view Details</a>
                            <a href="buyhp.php" class="btn btn-primary">Buy Now!</a>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

<section id="newProducts">
    <div class="container">
        <h4>Accessories</h4>
        <div class="row">
            <div class="col-lg-3">

                <div class="card" style="width: 17rem;">

                    <img class="card-img-top" src="img/acc.jpg" alt="accessories">
                    <div class="card-body">

                        <a href="A.php" class="btn btn-primary">SHOP NOW!</a>

                    </div>
                </div>

                <br>
            </div>
            <div class="col-lg-9">
                <div class="card-deck">
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

<section id="newProducts">
    <div class="container">
        <h4>Networking Products</h4>
        <div class="row">
            <div class="col-lg-3">

                <div class="card" style="width: 17rem;">

                    <img class="card-img-top" src="img/net1.jpg" alt="net product">
                    <div class="card-body">


                        <a href="N.php" class="btn btn-primary">SHOP NOW!</a>

                    </div>
                </div>

                <br>
            </div>
            <div class="col-lg-9">
                <div class="card-deck">
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                    <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

<section id="newProducts1">
    <div class="container">
        <h3>Collections</h3>
        <div class="card-group">

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural </p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">lead-in to additional content. This card h</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a n</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

        </div>
    </div>
</section>
<section id="newProducts1">
    <div class="container">

        <div class="card-group">

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural </p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">lead-in to additional content. This card h</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a n</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

            <div class="card">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div>
            </div>

        </div>
    </div>
</section>


</body>
<!-- Footer -->

<?php include_once '../views/elements/footer.php'  ?>

<?php include_once '../views/elements/script.php'  ?>
</body>
</html>